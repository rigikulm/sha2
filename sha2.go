// Prints the hash of standard input with optional sha384 and sha512.
// By default SHA-256 will be used.
package main

import (
	"crypto/sha256"
	"crypto/sha512"
	"flag"
	"fmt"
	"log"
)

type SHAOption int

const (
	SHA256 SHAOption = iota
	SHA384
	SHA512
)

func main() {
	var shaOpt SHAOption = SHA256

	// Process the command line arguments
	sha384Flag := flag.Bool("sha384", false, "generate a SHA-384 hash")
	sha512Flag := flag.Bool("sha512", false, "generate a SHA-512 hash")
	text := flag.String("t", "Hello World", "the text for which the hash is generated")
	flag.Parse()
	if *sha384Flag && *sha512Flag {
		log.Fatal("Error: Only select one SHA option")
	}

	if *sha384Flag {
		shaOpt = SHA384
	} else if *sha512Flag {
		shaOpt = SHA512
	}
	fmt.Println("SHAOpt--> ", shaOpt)

	// Convert the string to []byte
	textBytes := []byte(*text)
	switch shaOpt {
	case SHA256:
		h := sha256.Sum256(textBytes)
		fmt.Printf("Value: %s\n", *text)
		fmt.Printf("Hash: %x\n", h)
	case SHA384:
		h := sha512.Sum384(textBytes)
		fmt.Printf("Value: %s\n", *text)
		fmt.Printf("Hash: %x\n", h)
	case SHA512:
		h := sha512.Sum512(textBytes)
		fmt.Printf("Value: %s\n", *text)
		fmt.Printf("Hash: %x\n", h)
	}
}
