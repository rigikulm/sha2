# sha2
“Prints the hash of standard input with optional sha384 and sha512 hashes”

    This code is based on the similarly named programs from the book
    "The Go Programming Language" by Alan Donovan, and Brian Kernighan.

## Sample Programs

**sha2 usage**

``` bash
# default is SHA-256
$ sha2 -t "This is my message"
$
# Flag for SHA-512
$ sha2 -sha512 -t "This is another message"
```
